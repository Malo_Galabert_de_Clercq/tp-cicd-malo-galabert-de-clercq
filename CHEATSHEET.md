# Cheatsheet

Ce document répertorie les commandes dont vous aurez besoin durant ce TP (et qui ne sont pas relatives à Gitlab CI, bien entendu).

## Généralités sur Maven
### Lancement d'une commande Maven
Quelques astuces et commandes Maven utiles :

Résumé de l'utilisation de la CLI Maven :
```bash
mvn [options] phase[:task]
```

Options utiles pour les pipelines de CI :
- `--batch-mode` : Force le mode non-interactif (ne demande rien à l'utilisateur)
- `-Dstyle.color=always` : Active la coloration de la sortie standard (dont le rendu est effectué dans les sorties de logs des jobs Gitlab CI)
- `-Djansi.force=true` : Active la prise en compte des caractères d'échappement ANSI par la librairie [Jansi][1]

### Maven & Cycle de vie

Maven est un moteur de production dédié aux projets Java. Il est comparable à Ant ou make sous Linux.
Il permet de réaliser l'ensemble des étapes de production d'un projet à partir des fichiers sources.
La définition du projet Java est décrite dans le fichier pom.xml (Project Object Model).
Il permet de gérer le cycle de vie de génération de l'application :
- `validate` : On verifie la cohérence des informations de compilation
- `compile` : compilation du code source
- `test` : exécution des tests unitaires contenus dans le projet
- `package` : récupération du code compilé et création du package final (.jar) dans le répertoire target
- `install` : copie du package dans le répertoire maven local afin d'être disponible pour d'autre projets locaux
- `deploy` : copie du package sur le repertoire maven distant (Nexus, Artifactory, ..) pour le partager avec d'autres développeurs ou projets

**Important** (source Wikipédia): "l'idée est que, pour n'importe quel but, tous les buts en amont doivent être exécutés sauf s'ils ont déjà été exécutés avec succès et qu'aucun changement n'a été fait dans le projet depuis. Par exemple, quand on exécute mvn install, Maven va vérifier que mvn package s'est terminé avec succès (le jar existe dans target/), auquel cas cela ne sera pas ré-exécuté." **Chaque phase est donc importante dans le cadre d'un pipeline en succès**

## Partie 2
Les images OCI Maven sont disponible sur le Docker Hub en recherchant "maven" : https://hub.docker.com/_/maven.
Nous utiliserons ici Maven 3 avec OpenJDK 11. À vous de trouver l'image correspondante pour configurer les jobs de votre pipeline.

Lancement de la phase de compilation du code Java : lancement des phases `clean` et `compile`.
```bash
mvn clean compile
```

Lancement des tests unitaires avec [JUnit][2] : lancement de la phase `test`
```bash
mvn test
```

Construction du package `.jar` : lancement de la phase `package`
```bash
mvn package
```

Option Maven utile :
- `-DskipTests` : Ignore la phase de test dans le cycle de vie de l'application

## Partie 3
Maven génère ses fichiers (classes compilées, packages, etc.) dans le répertoire `target`.

Maven se base notamment sur la date de modification des fichiers .java et .class pour déterminer s'il doit lancer ou passer la phase de compilation. Les fichiers .java doivent avoir une date de modification postérieure aux fichiers .class pour que Maven recompile l'application.

Pour mettre à jour la date de dernière modification de tous les fichiers .class d'un répertoire nommé `mydir`, il est possible de lancer la commande suivante :
```bash
find mydir -name "*.class" -exec touch {} \;
```

Avec Maven, JUnit stocke ses rapports de test sous forme de fichiers `.xml` dans le répertoire `target/surefire-reports/TEST-*.xml`.

## Partie 4

### Plugin Maven SonarQube
**Phases maven à exécuter :** `verify sonar:sonar`

Les propriétés sont à passer au format options JVM (`-Dproperty.name=value`).

| Propriétés                | Description                               | Valeur exemple                                                        |
| ------------------------- | ----------------------------------------- | --------------------------------------------------------------------- |
| `sonar.host.url`          | L'URL de l'instance SonarCloud            | `https://sonarcloud.io/`                            |
| `sonar.projectKey`        | La clé de votre projet SonarCloud. Utilisez `<votre trigramme>` pour le TP.                | `LGR`                 |
| `sonar.projectName`       | Le nom d'affichage de votre projet SonarQube. Utilisez votre prénom (et votre nom si nécessaire). | `Léo`         |
| `sonar.gitlab.project_id` | L'id de votre projet Gitlab               | `42`                                                                  |
| `sonar.branch.name`       | Le nom de la branche en cours d'analyse   | `feature/my-feature-name`                                             |
| `sonar.java.binaries`     | Emplacement des classes java compilées    | `./target/classes`                                                    |
| `sonar.qualitygate.wait`  | Attend le résultat de l'analyse. Impacte le code retour du scan | `true`                                          |

**Note :** L'analyse est à exécuter une première fois sans le paramètre `sonar.branch.name` afin que Sonar puisse identifier la branche par défaut du projet. Si la première analyse est faite avec le paramètre `sonar.branch.name`, le message d'erreur suivant apparaîtra :
```
No branches currently exist in this project. Please scan the main branch without passing any branch parameters.
```

## Partie 5
**Note :** Il est possible de passer des options JVM à Maven (au format `-Doption.name=option.value`) sans les saisir dans toutes les commandes Maven, en utilisant la variable d'environnement `MAVEN_OPTS`, prise en charge par Maven. Exemple :
Sans `MAVEN_OPTS` :
```bash
mvn --batch-mode -Dstyle.color=always -Djansi.force=true compile
```
Avec `MAVEN_OPTS` :
```bash
MAVEN_OPTS="-Dstyle.color=always -Djansi.force=true"
mvn --batch-mode compile
```

Option Maven utile :
- `-Dmaven.repo.local=<path_to_dir>` : Définit le répertoire de cache local de Maven pour le téléchargement des dépendances, plugins, etc. 

> :warning: Maven attends un chemin absolu pour son répertoire local.

## Partie 6
Upload du package vers un repository
```bash
mvn deploy
```
GitLab détaille, dans ses pages d'aide, la manière d'uploader des packages sur ses registries. Cette documentation est accessible depuis le menu **Package & Registries** > **Package registry** de votre projet GitLab.

## Autres aides (pas nécessaire pour les TP)
### Injecter une variable d'environnement
Pour injector une variable d'environnement dans un fichier XML Maven (`settings.xml`, `pom.xml`), utiliser la notation `${env.MY_VARIABLE}`.

## Jib & Kaniko

`Kaniko`  : Il s'agit d'un outil (Open Source) qui va builder votre image de container à partir d'un Dockerfile. La construction de l’image Docker peut se faire dans un conteneur ou dans un cluster Kubernetes. Kaniko n’ayant pas besoin de Docker pour construire les images, la construction peut se faire en toute sécurité et peut faire partie du processus de déploiement.
```
🐳 docker_backend:
  stage: 💾 publish_in_registry
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    # Get version from Dockerfile
    - export VERSION=`grep "version" backend/build.gradle | head -1 | awk -F \' '{ print $2 }' | awk -F \' '{ print $1 }'`
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR//Dockerfile --destination gitlab.com/<<YOUR_PROJECT>>:$VERSION

```

`Jib` : Il s'agit d'un outil pour les projets Java qui va vous créer des conteneurs sans utiliser de fichier Dockerfile ni nécessiter l'installation de Docker. Vous pouvez utiliser Jib dans les plug-ins Jib pour Maven ou Gradle, ou utiliser la bibliothèque Java Jib. Jib gère toutes les étapes de l'empaquetage de votre application dans une image de conteneur. Vous n'avez pas besoin de connaître les bonnes pratiques pour créer des fichiers Dockerfile ou installer Docker. 
Jib organise votre application en plusieurs couches (dépendances, ressources et classes) et utilise la mise en cache des couches d'images Docker pour accélérer les compilations en recréant uniquement les modifications. L'organisation des couches de Jib et l'image de base de petite taille réduisent la taille globale de l'image, ce qui améliore les performances et la portabilité.
Il va fonctionner comme un plugin à ajouter dans votre fichier pom.xml (voir documentation officielle) et pourra être lancé comme une commande maven.

Exemple : (Innersource librarie !)
```
build-image:
  extends: .java-job-with-web-requests
  stage: build-image
  variables:
    BUILD_REGISTRY_USER: $CI_REGISTRY_USER
    BUILD_REGISTRY_PASSWORD: $CI_REGISTRY_PASSWORD
    BUILD_REGISTRY_IMAGE: $CI_REGISTRY_IMAGE
  image: ${CI_REGISTRY}/java-gradle:7.2-jdk11
  script:
    - gradle jib -s --console plain -Djib.to.image="$BUILD_REGISTRY_IMAGE:$BUILD_VERSION" -Djib.to.auth.username="$BUILD_REGISTRY_USER" -Djib.to.auth.password="$BUILD_REGISTRY_PASSWORD"
  interruptible: true
  rules:
    # exclude the job depending on some conditions
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_REF_NAME == $GITLAB_FLOW_PROD_BRANCH'
      when: never
    # include the job depending on some conditions
    - when: on_success
```

[1]: https://github.com/fusesource/jansi
[2]: https://junit.org/
